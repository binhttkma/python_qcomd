print("Viết chương trình trả ra từ điển với key là các số trong list, value là số lần xuất hiện của số trong list")
input_str = input("nhap mang so, cach nhau dau ,")
my_list= [int(s.strip()) for s in input_str.split(',')]  
my_dict = {}
for v in my_list:
    my_dict[v] = my_dict[v] + 1 if v in my_dict else 1
print(my_dict)