#Bài 1: Nhập số giây, chuyể sang tính số giờ phút giây
seconds = input("Nhập số giây: ")
seconds = int(seconds)
print("số giây là:" ,seconds)
hour = seconds // 3600
seconds -= hour * 3600
minute = seconds // 60
seconds -= minute * 60
print("=", hour, "Giờ",  minute,"Phút", seconds, "Giây")


#Bài 2: ĐỔi đơn vị nhiệt độ C sang các đơn vị nhiệt độ khác
doC = input("Nhập nhiệt độ C:")
doC= float(doC)
print( "Nhiệt độ ngoài trời đang là:", doC, "C")
doF = doC*9/5 +32
print( "Tương đương với nhiệt độ F:", doF)
doK = doC +273.15
print( "Tương đương với nhiệt độ K:", doK)
doR = round(doK*9/5, 2)
print( "Tương đương với nhiệt độ K:", doR)


#Bài 3: Đổi đơn vị đo lường độ dài
length= input("Nhập độ dài:")
length = float (length)
print("Độ dài vừa nhập là:", length)
length_m = round(length*1000, 2)
length_dm = round(length_m*10, 2)
length_cm = round(length_dm*10, 2)
length_mm = round(length_cm*10, 2)
length_mile = round(length * 1.609344, 2) 
length_inch = round(length * 39370.1, 2) 
print("",length_m,"m\n",length_dm,"dm\n",length_cm,"cm\n",length_mm,"mm\n",length_mile,"mile\n",length_inch,"inch")   