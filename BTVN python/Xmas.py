#Viết chương trình in ra thời gian đếm ngược đến XMas 2021 sau mỗi khoảng thời gian nhất định.
import datetime
import time

xmas_date = datetime.datetime(2021, 12, 25)
zero_delta = datetime.timedelta()

while True:
    remain = xmas_date - datetime.datetime.now() 
    if remain <= zero_delta:
        break
    print(remain)
    time.sleep(5)
