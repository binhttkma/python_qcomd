#Viết chương trình Python kiểm tra số lớn nhất trong 3 số
#Khai báo 3 biến a, b, c nhập giá trị số bất kỳ (int)
#Kiểm tra và in ra số lớn nhất trong 3 số đó
print ("Cách 1_Bài 1: tìm số lớn nhất trong 3 chữ số int nhập vào từ bàn phím dùng hàm max: ")
numberone = input("Mời nhập số thứ nhất: ")
numberone = int(numberone)
numbertwo = input("Mời nhập số thứ hai: ")
numbertwo = int( numbertwo)
numberthree = int(input ( "Mời nhập số thứ 3: "))
max( numberone,numbertwo)
max_number = max( numberone,numbertwo, numberthree)
print("Số thứ 1 là:" ,numberone)
print("Số thứ 2 là:" ,numbertwo)
print("Số thứ 3 là:" ,numberthree)
print ("Số lớn nhất trong dãy số (",numberone, numbertwo,numberthree, ") là", max_number)

#Cách 2 bài tìm số lớn nhất
print (" Cách 2_Tìm số lớn nhất trong 3 số nhập vào từ bàn phím, sử dụng câu lệnh điều kiện if else")
a = int(input(" Mời nhập số thứ 1: "))
b = int(input(" Mời nhập số thứ 2: "))
c = int(input(" Mời nhập số thứ 3: "))
print( "dãy số bạn nhập là:",a,b,c)
max_numbers = a if a > b else b
if max_numbers < c:
    max_numbers = c 
print('số lớn nhất là: ', max_numbers)

#Cách 3: TÌm số lớn nhất,đưa vào 1 mảng, tìm phần tử lớn nhất trong mảng vừa Nhập
print ( "Cách 3_Bài 1: dùng câu lệnh điều kiện tìm số nhỏ nhất và lớn nhất của 3 chữ số")
arrStr = input( " nhập số  cách nhau bằng dấu phẩy: ")
arr = arrStr.split(",")
arr = [int(x) for x in arr]
maxItem = arr[0]
for i in range(1,len(arr)):
    if arr[i] > maxItem:
        maxItem = arr[i]
print(arr, maxItem)
#Viết chương trình tính chỉ số BMI (Body Mass Index - Chỉ số cơ thể)
#Nhập chiều cao h (đơn vị m) và cân nặng w (đơn vị kg)
#Tính chỉ số BMI: w / (h * h)
#In chỉ số và thông báo kết quả theo quy ước:
#BMI < 17: Gầy độ II
#17 <= BMI < 18.5: Gầy độ I
#18.5 <= BMI < 25: Bình thường
#25 <= BMI < 30: Thừa cân
#30 <= BMI < 35: Béo phì độ I
#35 <= BMI: Béo phì độ II

print ("Bài 2: Viết chương trình tính chỉ số BMI (Body Mass Index - Chỉ số cơ thể)")
chieucao= input( "Mời nhập chiều cao của bạn ( m):")
chieucao= float( chieucao)
cannang = input ( "Mời nhập cân nặng:")
cannang = float (cannang) 
bmi_index = round (cannang / (chieucao * chieucao), 2)

if bmi_index < 17:
   print ("Chỉ số BMI của bạn là: ", bmi_index, "\nKết luận: Gầy độ II")
elif bmi_index >= 17 and bmi_index < 18.5:
   print ("Chỉ số BMI của bạn là: ", bmi_index, "\nKết luận: Gầy độ I")
elif bmi_index >= 18.5 and bmi_index < 25:
   print ("Chỉ số BMI của bạn là: ", bmi_index, "\nKết luận: Bình thường")
elif bmi_index >= 25 and bmi_index < 30:
   print ("Chỉ số BMI của bạn là: ", bmi_index, "\nKết luận: Thừa cân")
elif bmi_index >= 30 and bmi_index < 35:
   print ("Chỉ số BMI của bạn là: ", bmi_index, "\nKết luận: Béo phì độ I")   
elif bmi_index >= 35:
   print ("Chỉ số BMI của bạn là: ", bmi_index, "\nKết luận: Béo phì độ II")

#Viết chương trình kiểm tra một năm có phải năm nhuận hay không
#Nhập một năm year - int
#Kiểm tra và in ra kết quả year có phải năm nhuận hay không
#Năm nhuận là năm:
#Chia hết cho 400
#Chia hết cho 4 nhưng không chia hết cho 100
print ( "Bài 3: kiểm tra năm nhuận ")
nam = input ("Mời bạn nhập năm kiểm tra:")
nam = int(nam)
#print ( year_number)
if (nam % 400 == 0) or ((nam % 4==0) and (nam % 100 != 0)) :
    print(nam,"là năm nhuận")
else:
    print (nam,"không là năm nhuận")
