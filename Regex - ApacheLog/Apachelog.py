import re
import sys

"""
  Cho một file apache log
  In ra danh sach đường dẫn tới file .jpg]
"""
def path_names(filename):

  set_pathjpg = set()
  with open(filename, 'r') as f:
    for line in f:
      #print(type(line))
      pattern = "GET (.*\.jpg)"
      file_jpg = re.search(pattern,line)
      if file_jpg:
        s = file_jpg.groups()[0]
        set_pathjpg.add(s)
      else:
        continue
  print(set_pathjpg)


def main():
  if len(sys.argv) != 2:
    print('usage: ./Apachelog.py file')
    sys.exit(1)
  else:
    filename = sys.argv[1]
    path_names(filename)
    sys.exit(1)

if __name__ == '__main__':
  main()
