a_dict={}
string = "Hello World! 123"
lower = sum(map(str.islower, string))
upper= sum(map(str.isupper, string))
digit= sum(map(str.isdigit, string))
alpha= sum(map(str.isalpha, string))
def count_char_type(string):
    a_dict={"LETTER":alpha,"CASE": {"UPPER CASE":upper,"LOWER CASE":lower},"DIGIT":digit}
    return a_dict
print("Output: ",count_char_type(string))